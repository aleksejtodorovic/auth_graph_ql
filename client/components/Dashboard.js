import React from "react";

class Dashboard extends React.Component {
  render() {
    const { user } = this.props;

    if (!user) {
      return <div />;
    }

    return <div>Dashboard for: {user.email}</div>;
  }
}

export default Dashboard;
