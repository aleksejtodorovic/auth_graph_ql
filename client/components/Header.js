import React from "react";
import { graphql } from "react-apollo";
import { Link } from "react-router";

import query from "../queries/currentUser";
import logout from "../mutations/logout";

class Header extends React.Component {
  onLogoutClick = evt => {
    evt.preventDefault();

    this.props.mutate({
      refetchQueries: [{ query }]
    });
  };

  renderButtons() {
    const { loading, user } = this.props.data;

    if (loading) {
      return <div />;
    }

    if (user) {
      return (
        <li>
          <a onClick={this.onLogoutClick}>Logout</a>
        </li>
      );
    } else {
      return (
        <div>
          <li>
            <Link to="/signup">Signup</Link>
          </li>
          <li>
            <Link to="/login">Login</Link>
          </li>
        </div>
      );
    }
  }

  render() {
    return (
      <nav>
        <div className="nav-wrapper">
          <Link to="/dashboard" className="brand-logo left">
            Home
          </Link>
          <ul className="right">{this.renderButtons()}</ul>
        </div>
      </nav>
    );
  }
}

export default graphql(logout)(graphql(query)(Header));
