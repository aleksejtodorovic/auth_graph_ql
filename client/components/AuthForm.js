import React from "react";
import { reduce } from "async";

class AuthForm extends React.Component {
  state = { email: "", password: "" };

  onSubmit = evt => {
    evt.preventDefault();

    this.props.onSubmit(this.state);
  };

  render() {
    return (
      <div className="row">
        <form className="col s8" onSubmit={this.onSubmit}>
          <div className="input-field">
            <input
              placeholder="Email"
              type="email"
              value={this.state.email}
              onChange={({ target }) => this.setState({ email: target.value })}
            />
          </div>
          <div className="input-field">
            <input
              placeholder="Password"
              value={this.state.password}
              type="password"
              onChange={({ target }) =>
                this.setState({ password: target.value })
              }
            />
          </div>
          <div style={{ color: "red", fontSize: "16px", marginBottom: "10px" }}>
            {this.props.errors.map(error => (
              <div key={error}>{error}</div>
            ))}
          </div>
          <button className="btn">{this.props.buttonText}</button>
        </form>
      </div>
    );
  }
}

export default AuthForm;
