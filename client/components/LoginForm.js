import React from "react";
import { hashHistory } from "react-router";
import { graphql } from "react-apollo";
import AuthForm from "./AuthForm";

import login from "../mutations/login";
import fetchCurrentUser from "../queries/currentUser";

class LoginForm extends React.Component {
  state = { errors: [] };

  componentDidUpdate(prevProps) {
    if (!prevProps.data.user && this.props.data.user) {
      hashHistory.push("/dashboard");
    }
  }

  onSubmit = ({ email, password }) => {
    this.props
      .mutate({
        variables: {
          email,
          password
        },
        refetchQueries: [{ query: fetchCurrentUser }]
      })
      .catch(res => {
        const errors = res.graphQLErrors.map(error => error.message);
        this.setState({ errors });
      });
  };

  render() {
    const { loading } = this.props.data;

    if (loading) {
      return <div />;
    }

    return (
      <div className="container">
        <h3>Login</h3>
        <AuthForm
          onSubmit={this.onSubmit}
          errors={this.state.errors}
          buttonText="Login"
        />
      </div>
    );
  }
}

export default graphql(fetchCurrentUser)(graphql(login)(LoginForm));
