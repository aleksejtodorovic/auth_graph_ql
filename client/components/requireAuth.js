import React from "react";
import { hashHistory } from "react-router";
import { graphql } from "react-apollo";

import fetchCurrentUser from "../queries/currentUser";

export default WrappedComponent => {
  class RequireAuth extends React.Component {
    componentDidUpdate() {
      if (!this.props.data.loading && !this.props.data.user) {
        hashHistory.push("/login");
      }
    }

    render() {
      return <WrappedComponent {...this.props} user={this.props.data.user} />;
    }
  }

  return graphql(fetchCurrentUser)(RequireAuth);
};
